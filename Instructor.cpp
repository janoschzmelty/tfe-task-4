
#include "Instructor.h"

Instructor::Instructor(std::string prename, std::string surname, Birthday birthday, Address address, unsigned int registrationNumber)
	:Person(prename, surname, birthday, address), m_registrationNumber(registrationNumber)
{
}
unsigned int Instructor::getRegistrationNumber()
{
	return m_registrationNumber;
}