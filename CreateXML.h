#ifndef CREATEXML_H
#define CREATEXML_H

#include "Course.h"
#include "Serialize.h"

class CreateXML :public Serialize
{
public:
	virtual std::string create(Course);
};
#endif