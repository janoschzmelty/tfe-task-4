#ifndef ADDRESS_H
#define ADDRESS_H

#include <string>

//The class Adress contents the Adress Data of a Person
class Address
{
private:
	std::string m_street;
	unsigned int m_number;
	std::string m_city;
public:
	Address(std::string street, unsigned int number, std::string city);
};
#endif