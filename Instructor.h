#ifndef INSTRUCTOR_H
#define INSTRUCTOR_H

#include "Person.h"

class Instructor:public Person
{
private:
	unsigned int m_registrationNumber;
public:
	Instructor(std::string prename, std::string surname, Birthday birthday, Address address, unsigned int registrationNumber);
	unsigned int getRegistrationNumber();
};
#endif