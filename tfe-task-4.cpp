#include <string>
#include <vector>
#include "Course.h"
#include "CreateXML.h"
#include "CreateJSON.h"

int main()
{
	Course my_course(
		"tfe",
		Instructor("christian","ege",
			Birthday(1,1,1999),
			Address("allee",1,"friedrichshafen"),
			345678
		),
		std::vector<Student>({
			Student("max", "mustermann", Birthday(2, 3, 2000), Address("waldstrasse", 2, "berg"), 1, 569),
			Student("klaus", "mustermann", Birthday(2, 3, 2000), Address("waldstrasse", 2, "berg"), 2.3, 789),
			Student("fritz", "mustermann", Birthday(2, 3, 2000), Address("waldstrasse", 2, "berg"), 4.1, 123),
		})
	);
	CreateXML createXML;
	createXML.create(my_course);
	CreateJSON createJSON;
	createJSON.create(my_course);

	system("PAUSE");
}