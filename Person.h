#ifndef PERSON_H
#define PERSON_H



#include <string>
#include"Address.h"
#include "Birthday.h"


class Person
{
protected:
	std::string m_prename;
	std::string m_surname;
	Birthday m_birthday;
	Address m_address;
public:
	Person(std::string prename, std::string surname, Birthday birthday, Address address);
	std::string getPrename();
	std::string getSurname();

};
#endif