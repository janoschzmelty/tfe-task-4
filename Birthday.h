#ifndef BIRTHDAY_H
#define	BIRTHDAY_H

class Birthday
{
private:
	unsigned int m_day;
	unsigned int m_month;
	unsigned int m_year;
public:
	Birthday(unsigned int day, unsigned int month, unsigned int year);
};
#endif