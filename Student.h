#ifndef STUDENT_H
#define STUDENT_h

#include "Person.h"

class Student:public Person
{
private:
	float m_grade;
	float m_gradeRound;
	unsigned int m_matriculationNumber;
public:
	Student(std::string prename, std::string surname, Birthday birthday, Address address,float grade, unsigned int matriculationNumber);
	unsigned int getMatriculationNumber();
	float getGrade();
};
#endif