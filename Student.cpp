#include "Student.h"


Student::Student(std::string prename, std::string surname, Birthday birthday, Address address , float grade, unsigned int matriculationNumber)
	:Person(prename, surname, birthday, address),m_grade(grade), m_matriculationNumber(matriculationNumber)
{
}
unsigned int Student::getMatriculationNumber()
{
	return m_matriculationNumber;
}

float Student::getGrade()
{

	return m_grade;
}