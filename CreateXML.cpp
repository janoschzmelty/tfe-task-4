#include "CreateXML.h"
#include <string>
#include <iostream>
#include <fstream>

std::string CreateXML::create(Course course)
{
	//using helping string to read the vector

	std::string helpString;

	for (unsigned int i = 0; i < course.m_students.size(); i++)
	{
		helpString = helpString + "<student><matriculationnumber>" + std::to_string(course.m_students[i].getMatriculationNumber()) + "</matriculationnumber><grade>" + std::to_string(course.m_students[i].getGrade()).substr(0, 3) + "</grade></student>";
	}

	std::string results = "<?xml version=\"1.0\"?><instructor>"
		"<forename>" + course.getInstructor().getPrename() + "</forename>"
		"<lastname>" + course.getInstructor().getSurname() + "</lastname>"
		"<registrationnumber>" + std::to_string(course.getInstructor().getRegistrationNumber()) + "</registrationnumber>"
		"<exams><informatik3><students>" + helpString +
		"</students></informatik3></exams></instructor>";
	std::ofstream fout("tfe162.xml");
	fout << results;
	fout.close();
	return results;
}