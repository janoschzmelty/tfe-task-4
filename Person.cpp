#include "Person.h"

Person::Person(std::string prename, std::string surname, Birthday birthday, Address address)
	:m_prename(prename), m_surname(surname), m_birthday(birthday), m_address(address)
{
}

std::string Person::getPrename()
{
return m_prename;
}
std::string Person::getSurname()
{
	return m_surname;
}