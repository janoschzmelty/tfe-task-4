#include "CreateJSON.h"
#include <string>
#include <iostream>
#include <fstream>

std::string CreateJSON::create(Course course)
{
	//using helping string to read the vector

	std::string helpString;

	for (unsigned int i = 0; i < course.m_students.size(); i++)
	{
		helpString = helpString + ",{\"matriculationnumber\":" + std::to_string(course.m_students[i].getMatriculationNumber()) + ",\"grade\":" + std::to_string(course.m_students[i].getGrade()).substr(0, 3) + "}";
	}

	std::string results = "{ \"instructor\"{\"forename\":" + course.getInstructor().getPrename() + ", \"lastname\":" + course.getInstructor().getSurname() + ",\"registrationnumber\":" + std::to_string(course.getInstructor().getRegistrationNumber()) + ",\"<exams\":{ \"informatik3\":[" + helpString + "]}}}";
	std::ofstream fout("tfe162.json");
	fout << results;
	fout.close();
	std::cout << results << std::endl;
	return results;
}