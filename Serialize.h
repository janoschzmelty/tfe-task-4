#ifndef SERIALIZE_H
#define SERIALIZE_H

#include "Course.h"

class Serialize
{
public:
	virtual std::string create(Course)=0;
};

#endif