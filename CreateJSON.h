#ifndef CREATEJSON_H
#define CREATEJSON_h

#include "Serialize.h"

class CreateJSON :public Serialize
{
public:
	virtual std::string create(Course);
};
#endif