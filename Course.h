#ifndef COURSE_H
#define COURSE_H

#include <string>
#include <vector>
#include "Instructor.h"
#include "Student.h"

class Course
{
private:
	std::string m_name;
	Instructor m_instructor;
public:	
	std::vector <Student> m_students;
	Course(std::string name, Instructor instructor, std::vector<Student> students);
	Instructor getInstructor();
	//Student getStudent(Student);
};
#endif